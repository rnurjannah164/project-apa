/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.infra.model.SortOrder;
import com.github.adminfaces.starter.model.Car;
import com.github.adminfaces.template.exception.BusinessException;
import com.github.cintakode.starter.infra.GrailsRestClient;
import com.github.cintakode.starter.infra.LoginCinta;
import com.google.gson.Gson;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
//import java.util.function.Predicate;
//import java.util.stream.Collectors;

import static com.github.adminfaces.template.util.Assert.has;

/**
 * @author rmpestano
 *         Car Business logic
 */
@Stateless
public class CarService implements Serializable {

    @Inject
    List<Car> allCars;

    @Inject 
    GrailsRestClient  grailsRestClient;
    
    public List<Car> listByModel(String model) {
//        return allCars.stream()
//                .filter(c -> c.getModel().equalsIgnoreCase(model))
//                .collect(Collectors.toList());
        
        List<Car> l = new ArrayList<>();
        for(Car c : allCars) {
        		if (c.getModel().equalsIgnoreCase(model))
        			l.add(c);
        }
        return l;
    }

    public List<Car> paginate(Filter<Car> filter) {
        List<Car> pagedCars = new ArrayList<>();
        if(has(filter.getSortOrder()) && !SortOrder.UNSORTED.equals(filter.getSortOrder())) {
//                pagedCars = allCars.stream().
//                    sorted((c1, c2) -> {
//                        if (filter.getSortOrder().isAscending()) {
//                            return c1.getId().compareTo(c2.getId());
//                        } else {
//                            return c2.getId().compareTo(c1.getId());
//                        }
//                    })
//                    .collect(Collectors.toList());
            
        		for(Car c : allCars) {
        			pagedCars.add(c);
            }
        		 
        	
        }

        int page = filter.getFirst() + filter.getPageSize();
        if (filter.getParams().isEmpty()) {
            pagedCars = pagedCars.subList(filter.getFirst(), page > allCars.size() ? allCars.size() : page);
            return pagedCars;
        }
        
        List<Car> pagedList = allCars;
        		
//        List<Predicate<Car>> predicates = configFilter(filter);

//        List<Car> pagedList = allCars.stream().filter(predicates
//                .stream().reduce(Predicate::or).orElse(t -> true))
//                .collect(Collectors.toList());

//        if (page < pagedList.size()) {
//            pagedList = pagedList.subList(filter.getFirst(), page);
//        }

//        if (has(filter.getSortField())) {
//            pagedList = pagedList.stream().
//                    sorted((c1, c2) -> {
//                        boolean asc = SortOrder.ASCENDING.equals(filter.getSortOrder());
//                        if (asc) {
//                            return c1.getId().compareTo(c2.getId());
//                        } else {
//                            return c2.getId().compareTo(c1.getId());
//                        }
//                    })
//                    .collect(Collectors.toList());
//        }
        return pagedList;
    }

//    private List<Predicate<Car>> configFilter(Filter<Car> filter) {
//        List<Predicate<Car>> predicates = new ArrayList<>();
//        if (filter.hasParam("id")) {
//            Predicate<Car> idPredicate = (Car c) -> c.getId().equals(filter.getParam("id"));
//            predicates.add(idPredicate);
//        }
//
//        if (filter.hasParam("minPrice") && filter.hasParam("maxPrice")) {
//            Predicate<Car> minMaxPricePredicate = (Car c) -> c.getPrice()
//                    >= Double.valueOf((String) filter.getParam("minPrice")) && c.getPrice()
//                    <= Double.valueOf((String) filter.getParam("maxPrice"));
//            predicates.add(minMaxPricePredicate);
//        } else if (filter.hasParam("minPrice")) {
//            Predicate<Car> minPricePredicate = (Car c) -> c.getPrice()
//                    >= Double.valueOf((String) filter.getParam("minPrice"));
//            predicates.add(minPricePredicate);
//        } else if (filter.hasParam("maxPrice")) {
//            Predicate<Car> maxPricePredicate = (Car c) -> c.getPrice()
//                    <= Double.valueOf((String) filter.getParam("maxPrice"));
//            predicates.add(maxPricePredicate);
//        }
//
//        if (has(filter.getEntity())) {
//            Car filterEntity = filter.getEntity();
//            if (has(filterEntity.getModel())) {
//                Predicate<Car> modelPredicate = (Car c) -> c.getModel().toLowerCase().contains(filterEntity.getModel().toLowerCase());
//                predicates.add(modelPredicate);
//            }
//
//            if (has(filterEntity.getPrice())) {
//                Predicate<Car> pricePredicate = (Car c) -> c.getPrice().equals(filterEntity.getPrice());
//                predicates.add(pricePredicate);
//            }
//
//            if (has(filterEntity.getName())) {
//                Predicate<Car> namePredicate = (Car c) -> c.getName().toLowerCase().contains(filterEntity.getName().toLowerCase());
//                predicates.add(namePredicate);
//            }
//        }
//        return predicates;
//    }

    public List<String> getModels(String query) {
//        return allCars.stream().filter(c -> c.getModel()
//                .toLowerCase().contains(query.toLowerCase()))
//                .map(Car::getModel)
//                .collect(Collectors.toList());
        List<String> l = new ArrayList<>();
        for(Car c : allCars) {
        		if(c.getModel().toLowerCase().contains(query.toLowerCase()))
        			l.add(c.getModel());
        }
        return l;
    }

    public void insert(Car car) {
//        beforeInsert(car);

//        car.setId(allCars.stream()
//                .mapToInt(c -> c.getId())
//                .max()
//                .getAsInt()+1);
        
//        int max = 0;
//        for(Car c : allCars) {
//        		if (max < c.getId())
//        			max = c.getId();
//        }
//        car.setId(max+1);
//    	allCars.add(car);
    	
        String response = grailsRestClient.post("car/save", car);
        System.out.println(response);
        if (!response.equalsIgnoreCase("failed")) {
            Gson gson = new Gson();
            Car c = gson.fromJson(response, Car.class);
            car.setId(c.getId());
            allCars.add(car);
        }
        
    }

    public void beforeInsert(Car car) {
        if (!car.hasModel()) {
            throw new BusinessException("Car model cannot be empty");
        }
        if (!car.hasName()) {
            throw new BusinessException("Car name cannot be empty");
        }

//        if (allCars.stream().filter(c -> c.getName().equalsIgnoreCase(car.getName())
//                && c.getId() != c.getId()).count() > 0) {
//            throw new BusinessException("Car name must be unique");
//        }
        
        int count = 0;
        
        for(Car c : allCars) {
        		
        		if (c.getName().equalsIgnoreCase(car.getName()) && c.getId() != car.getId()) {
        			count++;
        		}	
        }
        
        if (count > 0) {
            throw new BusinessException("Car name must be unique");
        }
        
    }


    public void remove(Car car) {
    		//allCars.remove(car);
        
    		String response = grailsRestClient.delete("car/delete?id="+car.getId());
        System.out.println(response);
        if (!response.equalsIgnoreCase("failed")) {
        		allCars.remove(allCars.indexOf(car));
            Gson gson = new Gson();
            Car c = gson.fromJson(response, Car.class);
            allCars.add(car);
        }
    }

    public long count(Filter<Car> filter) {
//        return allCars.stream()
//                .filter(configFilter(filter).stream()
//                        .reduce(Predicate::or).orElse(t -> true))
//                .count();
        return 0;
    }

    public Car findById(Integer id) {
//        return allCars.stream()
//                .filter(c -> c.getId().equals(id))
//                .findFirst()
//                .orElseThrow(() -> new BusinessException("Car not found with id " + id));
        Car x = new Car();
        for(Car c : allCars) {
        		if(c.getId().equals(id)) 
        			x = c;
        	}
        return x;
    }

    public void update(Car car) {
    		System.out.println("update");
        String response = grailsRestClient.put("car/update", car);
        System.out.println(response);
        if (!response.equalsIgnoreCase("failed")) {
        		allCars.remove(allCars.indexOf(car));
            Gson gson = new Gson();
            Car c = gson.fromJson(response, Car.class);
            allCars.add(car);
        }
    }
    
}
