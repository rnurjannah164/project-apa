package com.github.adminfaces.starter.infra.security;

import com.github.adminfaces.template.config.AdminConfig;
import com.github.adminfaces.template.session.AdminSession;
import com.github.adminfaces.template.util.Constants;
import com.github.cintakode.starter.infra.GrailsRestClient;
import com.github.cintakode.starter.infra.LoginCinta;
import com.google.gson.Gson;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.github.adminfaces.starter.util.Utils.addDetailMessage;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login page or not.
 * By default AdminSession isLoggedIn always resolves to true so it will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user must be redirect to initial page or logon
 * you can skip this class.
 */
@Named
@SessionScoped
@Specializes
public class LogonMB extends AdminSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private String currentUser;
//    private String email;
//    private boolean remember;
    
	private String username;
    private String password;
    private String token;
    private List<String> roles;
    private boolean isOperator;
    private String nama;
    
    @Inject
    AdminConfig adminConfig;

    @Inject
    GrailsRestClient grailsRestClient;
    
//    @Inject
//    LoginCinta loginCinta;
    
    private Logger log = Logger.getLogger(LogonMB.class.getName());

    public void login() throws IOException {
    		
//    		log.log(Level.INFO, "Submitting login with username of {0} and password of {1}", new Object[]{this.loginCinta.getUsername(), this.loginCinta.getPassword()});
//        currentUser = loginCinta.getNama();
        
        log.log(Level.INFO, "Submitting login with username of {0} and password of {1}", new Object[]{this.username, this.password});
        
//        if (loginCinta.getUsername() != null && loginCinta.getPassword() != null) {
        	if (this.username != null && this.password != null) {
        		LoginCinta loginCinta = new LoginCinta();
        		loginCinta.setUsername(username);
        		loginCinta.setPassword(password);
        		loginCinta.setToken(token);
        		loginCinta.setRoles(roles);
        		loginCinta.setIsOperator(isOperator);
        		loginCinta.setNama(nama);
            String loginResponse = grailsRestClient.login(loginCinta);
            if (!loginResponse.equalsIgnoreCase("failed")) {
                Gson gson = new Gson();
                LoginCinta auth = gson.fromJson(loginResponse, LoginCinta.class);
                if (auth.getToken() != null && !auth.getToken().equals("")) {
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    Map<String, Object> sessionMap = externalContext.getSessionMap();
                    
                    setToken(auth.getToken());
                    setRoles(auth.getRoles());
                    setIsOperator(auth.isIsOperator());
                    setNama(auth.getNama());
                    
                    String validateResponse = grailsRestClient.validate(this.token);
                    
                    if (!validateResponse.equalsIgnoreCase("failed")) {
                    		LoginCinta validatedAuth = gson.fromJson(validateResponse, LoginCinta.class);
                        if (validatedAuth.getRoles() != null) {
                            setRoles(validatedAuth.getRoles());
                        }
                        sessionMap.put("username", this.username);
                        sessionMap.put("token", this.token);
                        sessionMap.put("roles", this.roles);
                        sessionMap.put("nama", this.nama);
//                        this.setLoggedIn(true);

//                        ret = "success";
                        
                        currentUser = this.nama;
                        
                        addDetailMessage("Logged in successfully as <b>" + currentUser + "</b>");
                        Faces.getExternalContext().getFlash().setKeepMessages(true);
                        Faces.redirect("index.xhtml");
                    } else {
                    		Faces.getExternalContext().getFlash().setKeepMessages(true);
                    		Messages.addFatal(null,"Login Failed: " + "Token tidak valid.");
                    }
                    
                    
                }
            } else {
//                message = new FacesMessage("Username/Password tidak sesuai", "Username dan/atau Password salah");
//                FacesContext.getCurrentInstance().addMessage(null, message);
//                ret = "failed";
                Faces.getExternalContext().getFlash().setKeepMessages(true);
                Messages.addFatal(null,"Login Failed: " + "Username dan/atau Password salah.");
            
                
                
            }
        } else {
//            message = new FacesMessage("Username/Password tidak sesuai", "Username dan/atau Password harus diisi");
//            FacesContext.getCurrentInstance().addMessage(null, message);
//            ret = "failed";
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Messages.addFatal(null,"Login Failed: " + "Username dan/atau Password harus diisi.");
        
        }
        
        
    }

    public void logout() throws IOException {
		String logoutResponse = grailsRestClient.logout(token);
		log.log(Level.INFO, "Logout Response with token of {0} and response of {1}", new Object[]{this.token, logoutResponse});
        
//        if ("200".equals(logoutResponse)) {
        	if (!logoutResponse.equalsIgnoreCase("failed")) {
                	
        		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	        Map<String, Object> sessionMap = externalContext.getSessionMap();
	        sessionMap.clear();
	        externalContext.invalidateSession();
        
	        String loginPage = adminConfig.getLoginPage();
	        
	        log.log(Level.INFO, "Response token {0} and response of {1}", new Object[]{this.token, logoutResponse});
            log.log(Level.INFO, "Login Page {0}", new Object[]{loginPage});
            
	        if (loginPage == null || "".equals(loginPage)) {
	            loginPage = Constants.DEFAULT_INDEX_PAGE;
	        }
	        if (!loginPage.startsWith("/")) {
	            loginPage = "/" + loginPage;
	        }
	        Faces.getSession().invalidate();
	        ExternalContext ec = Faces.getExternalContext();
	        
	        log.log(Level.INFO, "Redirect Page {0}", new Object[]{ec.getRequestContextPath() + loginPage});
            
	        ec.redirect(ec.getRequestContextPath() + loginPage);

        }
	}

    
    @Override
    public boolean isLoggedIn() {
//    		return grailsRestClient.validate(token);
//        return currentUser != null;
    		return token != null;
    }

//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
    
//    public boolean isRemember() {
//        return remember;
//    }
//
//    public void setRemember(boolean remember) {
//        this.remember = remember;
//    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public boolean isIsOperator() {
        return isOperator;
    }

    public void setIsOperator(boolean isOperator) {
        this.isOperator = isOperator;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

}
