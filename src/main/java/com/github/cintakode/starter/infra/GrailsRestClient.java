package com.github.cintakode.starter.infra;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
//import id.go.skkmigas.framework.util.SessionUtil;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;

@Named
@SessionScoped
public class GrailsRestClient implements Serializable {
    private static final long serialVersionUID = 1L;

    private String url;
    
    private Logger log = Logger.getLogger(GrailsRestClient.class.getName());

    @PostConstruct
    public void init() {
    		this.url =  "http://localhost:8081/prime-server/";
    }

//    public GrailsRestClient() {
//        try {
//            this.url = SessionUtil.getInitialParam("grailsEndPoint");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public GrailsRestClient(String url) {
//        this.url = url;
//    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private void handleError(Integer status) {
        String summary = null, detail = null;
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                summary, detail);

        if (status == 400) {
            summary = "http error 400";
        } else if (status == 401) {
            summary = "http error 401";
        } else if (status == 403) {
            summary = "http error 403";
        } else if (status == 404) {
            summary = "http error 404";
        } else if (status >= 500) {
            summary = "http error " + status;
        } else {
            summary = "error";
        }

        message.setSummary(summary);
        FacesContext.getCurrentInstance().addMessage(null, message);

    }

	public String login(Object post) {
    		log.log(Level.INFO, "Entering login with url of {0}", new Object[]{url});
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + "api/login");
            Gson gson = new Gson();
            String input = gson.toJson(post);
            log.log(Level.INFO, "Post input {0}", new Object[]{input});
            
            ClientResponse response = webResource.type("application/json")
                    .post(ClientResponse.class, input);

            if (response.getStatus() != 200) {
                handleError(response.getStatus());
                output = "failed";
            } else {
                output = response.getEntity(String.class);
            }
    			log.log(Level.INFO, "Response status {0}", new Object[]{response.getStatus()});
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
        }
        return output;
    }
	
	public String logout(String token) {
		log.log(Level.INFO, "Entering logout with url of {0}", new Object[]{url});
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + "api/logout");
            
            log.log(Level.INFO, "Post logout token {0}", new Object[]{token});
            
            ClientResponse response = webResource.type("application/json")
                    .header("X-Auth-Token", token).post(ClientResponse.class);
            
            log.log(Level.INFO, "Response logout with {0}", new Object[]{response});
            
            
            if (response.getStatus() != 200) {
                handleError(response.getStatus());
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
                output = "failed";
            } else {
//            		output = response.getEntity(String.class);
            		output = Integer.toString(response.getStatus());
            	}
            
            log.log(Level.INFO, "Response output with {0}", new Object[]{output});
            
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
        }
        return output;
    }
	
	public String validate(String token) {
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + "api/validate");

            ClientResponse response = webResource.type("application/json")
                    .header("X-Auth-Token", token).get(ClientResponse.class);

            if (response.getStatus() != 200) {
                handleError(response.getStatus());
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
                output = "failed";
            } else {
            		output = response.getEntity(String.class);
            }

            
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
        }
        return output;
    }

    public String getToken() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();

        return sessionMap.get("token").toString(); // use this when security enabled
        // return ""; //use this when security disabled
    }

    public String getUsername() {
        ExternalContext externalContext
                = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();

        return sessionMap.get("username").toString(); //use this when security enabled
        //  return ""; // use this when security disabled
    }

    public String get(String path) {
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(this.url + path);
            ClientResponse response = webResource.accept("application/json")
                    .header("X-Auth-Token", this.getToken())
                    .header("username", this.getUsername())
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
//                handleError(response.getStatus());
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus());
            }

            output = response.getEntity(String.class);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
        }
        return output;
    }

    public String post(String path, Object post)
            throws RuntimeException {
        
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(this.url + path);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();
            String input = gson.toJson(post);
            ClientResponse response = webResource.type("application/json")
                    .header("X-Auth-Token", this.getToken())
                    .header("username", this.getUsername())
                    .post(ClientResponse.class, input);

            if (response.getStatus() != 200) {
                handleError(response.getStatus());
                output = "failed";
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + response.getStatus() + "\n"
//                        + response.getEntity(String.class));
            } else {
                output = response.getEntity(String.class);
            }
        } catch (RuntimeException e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
        }
        return output;
    }

    public String put(String path, Object post) {
        String output = "";
        System.out.println("rest put ");
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(this.url + path);

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            String input = gson.toJson(post);
            System.out.println("rest input " + input);
            System.out.println("rest url " + url + path);
            ClientResponse response = webResource.type("application/json")
                    .header("X-Auth-Token", this.getToken())
                    .header("username", this.getUsername())
                    .put(ClientResponse.class, input);

            if (response.getStatus() != 200) {
                handleError(response.getStatus());
                output = "failed";
            } else {
                output = response.getEntity(String.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String delete(String path) throws RuntimeException {
        String output = "";
        Client client = Client.create();
        WebResource webResource = client.resource(url + path);
        try {
            ClientResponse response = webResource.accept("application/json")
                    .header("X-Auth-Token", this.getToken())
                    .header("username", this.getUsername())
                    .delete(ClientResponse.class);
            if (response.getStatus() != 200) {
                handleError(response.getStatus());
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            output = response.getEntity(String.class);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e
                            .getMessage(), ""));
            e.printStackTrace();
        }
        return output;
    }

    public String validateChangePassword(Object post) {
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + "api/login");
            Gson gson = new Gson();
            String input = gson.toJson(post);

            ClientResponse response = webResource.type("application/json")
                    .post(ClientResponse.class, input);

            if (response.getStatus() != 200) {
                output = "failed";
            } else {
                output = response.getEntity(String.class);
            }
        } catch (ClientHandlerException | UniformInterfaceException e) {
            e.printStackTrace();
        }
        return output;
    }

    public String upload(String path, InputStream is, String filename, String content_type, String folder, Long objectId) {
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + path);

            FormDataMultiPart fdmp = new FormDataMultiPart();
            FormDataBodyPart fdbp = new FormDataBodyPart(
                    FormDataContentDisposition.name("file").fileName(filename).build(),
                    is,
                    MediaType.valueOf(content_type));

            fdmp.field("folder", folder);
            fdmp.field("sutradaraId", objectId.toString());
            fdmp.bodyPart(fdbp);

            ClientResponse response = webResource.accept("application/json").type(MediaType.MULTIPART_FORM_DATA).header("X-Auth-Token", this.getToken()).header("username", this.getUsername())
                    .post(ClientResponse.class, fdmp);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            output = response.getEntity(String.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public InputStream download(String path, String filePath) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + path);

            ClientResponse response = webResource.queryParam("filePath", filePath).header("X-Auth-Token", this.getToken()).get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            return response.getEntityInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ClientResponse downloadFull(String path, String filePath) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(url + path);

            ClientResponse response = webResource.queryParam("filePath", filePath).header("X-Auth-Token", this.getToken()).get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
