package com.github.cintakode.starter.infra;

import java.io.Serializable;
import java.util.List;

//import javax.enterprise.context.SessionScoped;
//import javax.inject.Named;

//@Named
//@SessionScoped
public class LoginCinta implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String username;
    private String password;
    private String token;
    private List<String> roles;
    private boolean isOperator;
    private String nama;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public boolean isIsOperator() {
        return isOperator;
    }

    public void setIsOperator(boolean isOperator) {
        this.isOperator = isOperator;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
